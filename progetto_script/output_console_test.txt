AExprComplete: a +2*c - (4) + (a*a + 9*(a-c))
I(A, {'a': 2, 'A': 3, 'c': 5}) -> notRed = -15.0
I(A, {'a': 2, 'A': 3, 'c': 5}) -> red = -15.0

AExprEvaluationError: a +2*c
Variable c not in scope

AExprSyntaxError1: a +2*
Syntax error: 

AExprSyntaxError2: a +2*(-c)
Syntax error: (-c)

BExprEq1: x == 5
I(B, {'x': 5}) -> notRed = true
I(B, {'x': 5}) -> red = true

BExprEq2: x == 5
I(B, {'x': 4}) -> notRed = false
I(B, {'x': 4}) -> red = false

BExprNE: x |= 5
I(B, {'x': 5}) -> notRed = false
I(B, {'x': 5}) -> red = false

BExprLT: x < 5
I(B, {'x': 5}) -> notRed = false
I(B, {'x': 5}) -> red = false

BExprLE: x <= 5
I(B, {'x': 5}) -> notRed = true
I(B, {'x': 5}) -> red = true

BExprGT: x > 5
I(B, {'x': 5}) -> notRed = false
I(B, {'x': 5}) -> red = false

BExprGE: x >= 5
I(B, {'x': 5}) -> notRed = true
I(B, {'x': 5}) -> red = true

BExprTrue: true
I(B, {'x': 5}) -> notRed = true
I(B, {'x': 5}) -> red = true

BExprFalse: false
I(B, {'x': 5}) -> notRed = false
I(B, {'x': 5}) -> red = false

BExprNot: !x==5
I(B, {'x': 5}) -> notRed = false
I(B, {'x': 5}) -> red = false

BExprParenthesis: (!false)
I(B, {'x': 5}) -> notRed = true
I(B, {'x': 5}) -> red = true

BExprAndFalse: true && x|=5
I(B, {'x': 5}) -> notRed = false
I(B, {'x': 5}) -> red = false

BExprAndTrue: true && false
I(B, {'x': 5}) -> notRed = false
I(B, {'x': 5}) -> red = false

BExprOr: false || true
I(B, {'x': 5}) -> notRed = true
I(B, {'x': 5}) -> red = true

BExprPrecedenceAnd: true && false && false || true
I(B, {'x': 5}) -> notRed = true
I(B, {'x': 5}) -> red = true

BExprSyntaxError: true && false && || true
Syntax error: || true

BExprOrShortCircuit: true || a==7
I(B, {'x': 5}) -> notRed = true
I(B, {'x': 5}) -> red = true

BExprANdShortCircuit: false && a==7
I(B, {'x': 5}) -> notRed = false
I(B, {'x': 5}) -> red = false

BExprImpliesFalse: true -> false
I(B, {'x': 5}) -> notRed = false
I(B, {'x': 5}) -> red = false

BExprImpliesTrue: false -> true
I(B, {'x': 5}) -> notRed = true
I(B, {'x': 5}) -> red = true

BExprDoubleImpliesFalse: false <-> true
I(B, {'x': 5}) -> notRed = false
I(B, {'x': 5}) -> red = false

BExprDoubleImpliesTrue: false <-> false
I(B, {'x': 5}) -> notRed = true
I(B, {'x': 5}) -> red = true

BExprComplete: x > 0 && ( true || 4 < 7 ) && (!87>=32.09 || x==2) -> x|=2
I(B, {'x': 2}) -> notRed = false
I(B, {'x': 2}) -> red = false

AssignStm: a := 5
I(S, {'x': 2}) -> notRed = {'x': 2, 'a': 5.0}
I(S, {'x': 2}) -> red = {'x': 2, 'a': 5.0}

skipStm: skip
I(S, {'x': 2}) -> notRed = {'x': 2}
I(S, {'x': 2}) -> red = {'x': 2}

ifTrueStm: if true then a:=1 else a:=2 end
I(S, {'x': 2}) -> notRed = {'x': 2, 'a': 1.0}
I(S, {'x': 2}) -> red = {'x': 2, 'a': 1.0}

ifFalseStm: if false then a:=1 else a:=2 end
I(S, {'x': 2}) -> notRed = {'x': 2, 'a': 2.0}
I(S, {'x': 2}) -> red = {'x': 2, 'a': 2.0}

infiniteLoopStm: while true do skip end
whileErrorReductionStm: while x<1500 do x := x+1 end
I(S, {'x': 0}) -> notRed = {'x': 1500.0}
Traceback (most recent call last):
  File "_pydevd_bundle/pydevd_cython.pyx", line 1557, in _pydevd_bundle.pydevd_cython.ThreadTracer.__call__
RecursionError: maximum recursion depth exceeded
maximum recursion depth exceeded during compilation

whileStm: while x<3 do x := x+1 end
I(S, {'x': 0}) -> notRed = {'x': 3.0}
I(S, {'x': 0}) -> red = {'x': 3.0}

repeatStm: repeat x := x * (x+1) until x>10
I(S, {'x': 1}) -> notRed = {'x': 42.0}
I(S, {'x': 1}) -> red = {'x': 42.0}

forStm: for tmpVar0 := 1 to tmpVar0+10 do x := x + tmpVar0 end
I(S, {'x': 0}) -> notRed = {'x': 55.0, 'tmpVar0': 10.0}
I(S, {'x': 0}) -> red = {'x': 55.0, 'tmpVar0': 10.0, 'tmpVar1': 11.0}

forPotentiallyInfiniteStm: for x := 1 to 10 do x := x -1 end
I(S, {'x': 0}) -> notRed = {'x': 8.0}
I(S, {'x': 0}) -> red = {'x': 8.0, 'tmpVar0': 10.0}

strangeFactorial:  initial := 1; final := 10; correctFactorial := 1; oddFactorial := 1; isOdd := 1; for i := initial to final+1 do skip; if isOdd == 1 then oddFactorial := oddFactorial * i else skip; skip end; isOdd := 1- isOdd; tmpCorrectFactorial := 0; ii := 0; while ii < i do tmpCorrectFactorial := tmpCorrectFactorial + correctFactorial; ii := ii+1 end; correctFactorial := tmpCorrectFactorial end
I(S, {}) -> notRed = {'initial': 1.0, 'final': 10.0, 'correctFactorial': 3628800.0, 'oddFactorial': 945.0, 'isOdd': 1.0, 'i': 10.0, 'tmpCorrectFactorial': 3628800.0, 'ii': 10.0}
I(S, {}) -> red = {'initial': 1.0, 'final': 10.0, 'correctFactorial': 3628800.0, 'oddFactorial': 945.0, 'isOdd': 1.0, 'i': 10.0, 'tmpVar0': 11.0, 'tmpCorrectFactorial': 3628800.0, 'ii': 10.0}