Input:
false && a==7

Tree parsed:
BExp[
	Bool:False
	 && 
	ABExp[
		Var:a
		 == 
		Num:7.0
	]
]


Remain string:
**Nothing**

Expression:
B: BExp[Bool:False && ABExp[Var:a == Num:7.0]]

Execution whit reduce to simple case:
I(B, {'x': 5}) -> red = false

B[[BExp[Bool:False && ABExp[Var:a == Num:7.0]]]] {'x': 5} = false
	B[[Bool:False]] {'x': 5} = false
