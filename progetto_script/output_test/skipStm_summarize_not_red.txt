Input:
skip

Tree parsed:
Stm[
	skip
]


Remain string:
**Nothing**

Expression:
S: Stm[skip]

Execution without reduce to simple case:
I(S, {'x': 2}) -> notRed = {'x': 2}

Sds[[Stm[skip]]] {'x': 2} = {'x': 2}
	Sds[[skip]] {'x': 2} = {'x': 2}
