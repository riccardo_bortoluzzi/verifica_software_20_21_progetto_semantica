initial := 1;
final := 10;
correctFactorial := 1;
oddFactorial := 1;
isOdd := ((1));
for i := initial to final+1
	do
		skip;
		if isOdd == 1
			then
				oddFactorial := oddFactorial * i
			else
				skip;
				skip
		end;
		isOdd := 1- isOdd;
		tmpCorrectFactorial := 0;
		ii := 0;
		while ii < i
			do 
				tmpCorrectFactorial := tmpCorrectFactorial + correctFactorial;
				ii := (ii+1)
		end;
		correctFactorial := tmpCorrectFactorial
end