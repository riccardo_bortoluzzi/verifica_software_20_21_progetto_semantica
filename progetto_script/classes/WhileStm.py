#script to implement the while statement

from __future__ import annotations
from classes.ExpressionBaseElement import ExpressionBaseElement
from classes.StatementBaseElement import StatementBaseElement
from typing import Dict, List, Set, Union
from classes.BoolExpr import BoolExpr
import classes.Statement as Statement
import classes.AtomStatement as AtomStatement
import classes.SkipStm as SkipStm
import classes.IfStm as IfStm

class WhileStm(StatementBaseElement):
	guard : BoolExpr
	stm : Statement.Statement

	numberOfExecution : int #number of execution, for the Kleene-Knaster-Tarski

	def __init__(self, guard:BoolExpr, stm:Statement.Statement) -> None:
		#method to inizialize the object and populate the attributes
		super().__init__()
		self.guard = guard
		self.stm = stm
		self.numberOfExecution = 0

	def toStringTree(self, nTab=0) -> str:
		#method to convert the while statement to a string
		#start with the number of tabs
		baseTab = '\t'*nTab
		stringTree = baseTab + 'while ' + '\n'
		stringTree += self.guard.toStringTree(nTab=nTab+1)
		stringTree += baseTab + ' do ' + '\n'
		stringTree += self.stm.toStringTree(nTab=nTab+1)
		return stringTree

	def findVariableSet(self) -> Set[str]:
		#method to find all variables names
		variableSet = self.guard.findVariableSet()
		variableSet.update(self.stm.findVariableSet())
		return variableSet

	def execute(self, state: Dict[str, float], listShowWhile: Union[List[str], None], listExecState: Union[List[str], None], flagReduction: bool, variableSet: Set[str]) -> None:
		#when the while statement is executed, the number of execution is alway incremented by 1
		self.numberOfExecution += 1
		#now distinguish if it will be executed with or not the Reduction 
		if flagReduction:
			self.executeReduction(state=state, listShowWhile=listShowWhile, listExecState=listExecState, variableSet=variableSet)
		else:
			self.executeNotReduction(state=state, listShowWhile=listShowWhile, listExecState=listExecState, variableSet=variableSet)

	def executeReduction(self, state: Dict[str, float], listShowWhile: Union[List[str], None], listExecState: Union[List[str], None], variableSet: Set[str]) -> None:
		#with this execution the while statement will not run forever because it is possible to raise a maximun recursion error, but it is reduced -> transform the while statement in a if condition
		#to transform in a if condition I nedd a new statement object
		listStatements = self.stm.listStms[:] #copy the list of atomic statements of the inner statement
		#add the atom statement that represent the while
		atomWhileStatement = AtomStatement.AtomStatement(self) #use the same while statement to increment the number of execution
		listStatements.append(atomWhileStatement)
		#create the new statement object
		newStatementWhile = Statement.Statement(listStatements)
		#create the statement for the skip
		skipStatement = SkipStm.SkipStm()
		atomSkipStatement = AtomStatement.AtomStatement(skipStatement)
		newStatementSkip = Statement.Statement([atomSkipStatement])
		#now create the if object
		newIfStatement = IfStm.IfStm(guard=self.guard, stmThen=newStatementWhile, stmElse=newStatementSkip)
		equivalentAtomIfStatement = AtomStatement.AtomStatement(newIfStatement)
		#execute the if clause
		equivalentAtomIfStatement.executionProcess(state=state, listShowWhile=listShowWhile, listExecState=listExecState, flagReduction=True, variableSet=variableSet, flagAtomic=False)

	def executeNotReduction(self, state: Dict[str, float], listShowWhile: Union[List[str], None], listExecState: Union[List[str], None], variableSet: Set[str]) -> None:
		#this execution try to avoid maximum recursion error and run forever, but not respect all rules of denotationational semantic (transform the while statement into a if statement)
		while self.guard.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalBExpr, flagReduction=False, flagAtomic=False):
			#increment thye number of execution
			self.numberOfExecution += 1
			self.stm.executionProcess(state=state, listShowWhile=listShowWhile, listExecState=listExecState, flagReduction=False, variableSet=variableSet, flagAtomic=False)

	#########################################################################################################################################################
	#method for graphic 

	def toGraphicTree(self):
		#method to convert the repeat until statement to a graphic tree
		from PyQt5 import QtWidgets
		graphicTree = QtWidgets.QTreeWidgetItem()
		graphicTree.setText(0, 'WhileDoStm')
		whileChildTree = QtWidgets.QTreeWidgetItem()
		whileChildTree.setText(0, 'while')
		guardChildTree = self.guard.toGraphicTree()
		whileChildTree.addChild(guardChildTree)
		doChildTree = QtWidgets.QTreeWidgetItem()
		doChildTree.setText(0, 'do')
		childTreeStm =  self.stm.toGraphicTree()
		doChildTree.addChild(childTreeStm)
		graphicTree.addChild(whileChildTree)
		graphicTree.addChild(doChildTree)
		return graphicTree