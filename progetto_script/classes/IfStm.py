#script to implement the if statement 

from __future__ import annotations
from classes.ExpressionBaseElement import ExpressionBaseElement
from typing import Dict, List, Set, Union
from classes.StatementBaseElement import StatementBaseElement

from classes.BoolExpr import BoolExpr
import classes.Statement as Statement

class IfStm(StatementBaseElement):
	guard : BoolExpr
	stmThen : Statement.Statement #the statement of the then branch
	stmElse : Statement.Statement #statement of the else branch

	def __init__(self, guard:BoolExpr, stmThen:Statement.Statement, stmElse:Statement.Statement) -> None:
		#method to inizialize the object with the guard and the branches
		super().__init__()
		self.guard = guard
		self.stmThen = stmThen
		self.stmElse = stmElse

	def toStringTree(self, nTab=0) -> str:
		#method to convert the if statement to a string
		#start with the number of tabs
		baseTab = '\t'*nTab
		stringTree = baseTab + 'if ' + '\n'
		stringTree += self.guard.toStringTree(nTab=nTab+1)
		stringTree += baseTab + ' then ' + '\n'
		stringTree += self.stmThen.toStringTree(nTab=nTab+1)
		stringTree += baseTab + ' else ' + '\n'
		stringTree += self.stmElse.toStringTree(nTab=nTab+1)
		return stringTree

	def findVariableSet(self) -> Set[str]:
		variableSet = self.guard.findVariableSet()
		variableSet.update(self.stmThen.findVariableSet())
		variableSet.update(self.stmElse.findVariableSet())
		return variableSet

	def execute(self, state: Dict[str, float], listShowWhile: Union[List[str], None], listExecState: Union[List[str], None], flagReduction: bool, variableSet: Set[str]) -> None:
		#method to execute the if statement
		#it is the same with Reduction mode and without it
		#first: check the guard
		boolGuard = self.guard.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalBExpr, flagReduction=flagReduction, flagAtomic=False)
		if boolGuard:
			self.stmThen.executionProcess(state=state, listShowWhile=listShowWhile, listExecState=listExecState, flagReduction=flagReduction, variableSet=variableSet, flagAtomic=False)
		else:
			self.stmElse.executionProcess(state=state, listShowWhile=listShowWhile, listExecState=listExecState, flagReduction=flagReduction, variableSet=variableSet, flagAtomic=False)

	#########################################################################################################################################################
	#method for graphic 

	def toGraphicTree(self):
		#method to convert the if statement to a graphic tree
		from PyQt5 import QtWidgets
		graphicTree = QtWidgets.QTreeWidgetItem()
		graphicTree.setText(0, 'IfStm')
		ifChildTree = QtWidgets.QTreeWidgetItem()
		ifChildTree.setText(0, 'if')
		guardChildTree = self.guard.toGraphicTree()
		ifChildTree.addChild(guardChildTree)
		thenChildTree = QtWidgets.QTreeWidgetItem()
		thenChildTree.setText(0, 'then')
		thenStmChildTree = self.stmThen.toGraphicTree()
		thenChildTree.addChild(thenStmChildTree)
		elseChildTree = QtWidgets.QTreeWidgetItem()
		elseChildTree.setText(0, 'else')
		elseStmChildTree = self.stmElse.toGraphicTree()
		elseChildTree.addChild(elseStmChildTree)
		#add children
		graphicTree.addChildren([ifChildTree, thenChildTree, elseChildTree])
		return graphicTree