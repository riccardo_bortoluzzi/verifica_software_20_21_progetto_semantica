#script to implement the binary and boolean expression

from __future__ import annotations
from typing import Dict, List, Set, Union
from classes.ExpressionBaseElement import ExpressionBaseElement

import classes.AtomBoolExpr as AtomBoolExpr

class AndBoolExpr(ExpressionBaseElement):
	leftBoolExpr : AtomBoolExpr.AtomBoolExpr
	rightBoolExpr : Union[None, AndBoolExpr]

	def __init__(self, leftBExpr:AtomBoolExpr.AtomBoolExpr, rightBExpr:Union[None, AndBoolExpr] = None) -> None:
		#constructor method that simply populate attributes
		super().__init__()
		self.leftBoolExpr = leftBExpr
		self.rightBoolExpr = rightBExpr

	def toStringTree(self, nTab=0) -> str:
		#method to convert the boolean expression to a string
		#start with the number of tabs
		baseTab = '\t'*nTab
		#verify the length of the expression
		if self.rightBoolExpr is None:
			#only 1 expression -> not increment indent
			stringTree = self.leftBoolExpr.toStringTree(nTab=nTab)
		else:
			#more tha 1 expression
			stringTree = baseTab + 'BExp[' + '\n'
			stringTree += self.leftBoolExpr.toStringTree(nTab=nTab+1)
			stringTree += baseTab + '\t' +' && ' + '\n'
			stringTree += self.rightBoolExpr.toStringTree(nTab=nTab+1)
			stringTree += baseTab + ']' + '\n'
		return stringTree

	def findVariableSet(self) -> Set[str]:
		variableSet = self.leftBoolExpr.findVariableSet()
		assert variableSet is not None
		if self.rightBoolExpr is not None:
			variableSet.update(self.rightBoolExpr.findVariableSet())
		return variableSet

	def evaluate(self, state: Dict[str, float], listExecState: Union[List[str], None], flagReduction: bool) -> bool:
		#method to evaluate the and boolean expression with the short circuit of and
		#the flag Reduction is ininfluent because the execution is the same
		#evaluate the left expression
		isAtomic = self.rightBoolExpr is None
		leftValue = self.leftBoolExpr.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalBExpr, flagAtomic=isAtomic, flagReduction=flagReduction)
		#check if it is necessary to evaluate also the second statement
		if not(isAtomic) and leftValue: #the left is true
			#evaluate the right statement
			assert self.rightBoolExpr is not None
			rightValue = self.rightBoolExpr.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalBExpr, flagAtomic=isAtomic, flagReduction=flagReduction)
			return bool(rightValue)
		#otherwise can return simply the left value
		return bool(leftValue)

	#########################################################################################################################################################
	#method for graphic 

	def toGraphicTree(self):
		#method to convert the boolean expression to a graphic tree
		from PyQt5 import QtWidgets
		#verify the length of the expression
		if self.rightBoolExpr is None:
			#only 1 expression -> not increment indent
			graphicTree = self.leftBoolExpr.toGraphicTree()
		else:
			#more tha 1 expression
			graphicTree = QtWidgets.QTreeWidgetItem()
			graphicTree.setText(0, 'BExp')
			firstChildTree = self.leftBoolExpr.toGraphicTree()
			graphicTree.addChild(firstChildTree)
			opChild = QtWidgets.QTreeWidgetItem()
			opChild.setText(0, '&&')
			graphicTree.addChild(opChild)
			secondChildTree = self.rightBoolExpr.toGraphicTree()
			graphicTree.addChild(secondChildTree)
		return graphicTree