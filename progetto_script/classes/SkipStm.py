#script to implement the class of the skip command

from __future__ import annotations
from typing import Dict, List, Set, Union
from classes.StatementBaseElement import StatementBaseElement

class SkipStm(StatementBaseElement):
	
	def __init__(self) -> None:
		#method to inizialize an object of type skip: nothing to do
		super().__init__()

	def toStringTree(self, nTab=0) -> str:
		#method to convert the skip statement to a string
		#start with the number of tabs
		baseTab = '\t'*nTab
		stringTree = baseTab + 'skip' + '\n'
		return stringTree

	def findVariableSet(self) -> Set[str]:
		return set()

	def execute(self, state: Dict[str, float], listShowWhile: Union[List[str], None], listExecState: Union[List[str], None], flagReduction: bool, variableSet: Set[str]) -> None:
		#the execution of the skip is only pass
		pass

	#########################################################################################################################################################
	#method for graphic 

	def toGraphicTree(self):
		#method to convert the skip statement to a graphic tree
		from PyQt5 import QtWidgets
		graphicTree = QtWidgets.QTreeWidgetItem()
		graphicTree.setText(0, 'SkipStm')
		return graphicTree