#script for the main class of the graphic object

from typing import Dict, List, Set, Tuple, Union
from PyQt5 import QtCore, QtGui, QtWidgets

import os, re
from classes.ArithmExpr import ArithmExpr
from classes.BoolExpr import BoolExpr
from classes.ExpressionBaseElement import ExpressionBaseElement
from classes.Parser import Parser
from classes.Statement import Statement

from classes.Ui_MainWindowSemantics import Ui_MainWindowSemantics
from classes.Constants import constDirOut, constNameParserTree, constNameSummarizeExecutionNotRed, constNameSummarizeExecutionRed


constTypeError = 0
constTypeInfo = 1

constStringInitialState = 'Initial State'
constStringFinalState = 'Final State'


class MainWindowSemantics(QtWidgets.QMainWindow):
	#estende la classe di qt

	ui : Ui_MainWindowSemantics

	def __init__(self) -> None:
		super().__init__()

		self.ui = Ui_MainWindowSemantics()
		self.ui.setupUi(self)

	##################################################################################################################################################################
	#general methods
	def readFile(self):
		#method to read the input from a file
		try:
			dlg = QtWidgets.QFileDialog()
			dlg.setFileMode(QtWidgets.QFileDialog.ExistingFile)
			dlg.setDirectory('.')
			if dlg.exec_():
				#delete all in the text
				self.ui.plainTextEditStringToParse.clear()
				filename = dlg.selectedFiles()
				with open(filename[0], 'r') as f:
					self.ui.plainTextEditStringToParse.insertPlainText(f.read())
		except Exception as e:
			self.showMessage(type_message=constTypeError, text=f'Error read file: {e}')

	def addVariable(self):
		#method to add a row in the table of variable
		self.ui.tableWidgetVariableInput.setRowCount( self.ui.tableWidgetVariableInput.rowCount() +1 )
		
	def removeVariable(self):
		#method to remove a variable
		self.ui.tableWidgetVariableInput.removeRow(self.ui.tableWidgetVariableInput.currentRow())
		
	def parseAndExecution(self):
		#method to parse the string and execute it
		self.processParseExecution(flagExecution=True)
	
	def parseOnly(self):
		#method to only parse the expression
		self.processParseExecution(flagExecution=False)
	
	def processParseExecution(self, flagExecution:bool):
		#function to execute the parse and the execution
#		try:
		if 1:
			#prepare the parameters
			#parse the string
			(strInput, typeParser, tree, remain) = self.parseStringInput()
			#create the string for the files
			assert tree is not None
			graphicTree = tree.toGraphicTree()
			#inizialize variables for the execution
			completeStringNotRed = ''
			completeStringRed = ''
			dictFinalStateStr = dict()
			dictInitialStateFloat = dict()
			listExecNotRed = []
			listExecRed = []
			listWhile = []
			#check if it is necessary the execution
			if flagExecution:
				#prepare the initial state
				dictInitialStateFloat = self.getInitialState()
				#prepare the execution
				typeRed = self.getSelectedRadioGroupBox(groupBox=self.ui.groupBoxTypeReduction).text()
				

				finalResult = None
				statePostExecution = dict()
				if typeRed in (self.ui.radioButtonRedBoth.text(), self.ui.radioButtonRedMinSteps.text()):
					#without Reduction flag
					(finalResult, listExecNotRed, listWhile, statePostExecution, completeStringNotRed) = self.evaluateTree(tree=tree, iniSt=dictInitialStateFloat, iniVarSet=set(), flagRed=False, typeParserGraphic=typeParser)
				#try execute with reduction because it could raise error of maximum depth
				try:
					if typeRed in (self.ui.radioButtonRedBoth.text(), self.ui.radioButtonRedSimple.text()):
						#the evaluation with Reduction reductions
						initialVariableSet = tree.findVariableSet()
						(finalResult, listExecRed, listWhile, statePostExecution, completeStringRed) = self.evaluateTree(tree=tree, iniSt=dictInitialStateFloat, iniVarSet=initialVariableSet, flagRed=True, typeParserGraphic=typeParser)
				except Exception as e:
					self.showMessage(type_message=constTypeError, text=f'Error in execution with reduction: {e}')
				#create the dict for final state
				dictFinalStateStr = self.copyDictFlotToStr(dictIn=statePostExecution)
				#populate final state if an aritmetic expression or a boolena expression is parsed
				if typeParser in (self.ui.radioButtonTypeAritExpr.text(), self.ui.radioButtonTypeBoolExpr.text()):
					dictFinalStateStr['result'] = str(finalResult).lower()
			#at the end, the output strings are populated
			self.ui.lineEditInputString.setText(re.sub(pattern=r'\s+', repl=' ', string=strInput))
			self.ui.lineEditRemainString.setText(('**Nothing**' if remain=='' else re.sub(pattern=r'\s+', repl=' ', string=remain)))
			self.ui.lineEditParsedExpression.setText(tree.toPlainString())
			self.ui.treeWidgetParsedTree.clear()
			self.ui.treeWidgetParsedTree.addTopLevelItem(graphicTree)
			self.ui.treeWidgetParsedTree.expandAll()
			#create the files
			self.createFiles(tree=tree, strInput=strInput, remain=remain, typeParserGraphic=typeParser, completeStringNotRed=completeStringNotRed, completeStringRed=completeStringRed)
			#insert state
			self.insertVariablesIntoTable(dictVar=self.copyDictFlotToStr(dictIn=dictInitialStateFloat), graphicTable=self.ui.tableWidgetInitialState)
			self.insertVariablesIntoTable(dictVar=dictFinalStateStr, graphicTable=self.ui.tableWidgetFinalState)
			#insert value in kleene knaster taski
			self.insertKleeneKnasterTarski(listWhile=listWhile)
			#insert tree execution
			self.insertExecutionStepIntoTree(listExecution=listExecNotRed, treeWidget=self.ui.treeWidgetExecutionNoReduction)
			self.insertExecutionStepIntoTree(listExecution=listExecRed, treeWidget=self.ui.treeWidgetExecutionReduction)
			#move to second page
			self.ui.toolBoxPage.setCurrentIndex(1)
			#inform the user
			self.showMessage(type_message=constTypeInfo, text='Process Completed')
#		except Exception as e:
#			self.showMessage(type_message=constTypeError, text=f'Error parse: {e}')

	################################################################################################################################################################
	#methods to show messages

	def showMessage(self, type_message:int, text:str):
		#method tho show a messagebox
		msg = QtWidgets.QMessageBox()
		if type_message == constTypeError:
			msg.setIcon(QtWidgets.QMessageBox.Critical)
			msg.setWindowTitle("Error")
		elif type_message == constTypeInfo:
			msg.setIcon(QtWidgets.QMessageBox.Information)
			msg.setWindowTitle("Info")

		msg.setText(text)
		#msg.setInformativeText("This is additional information")
		
		#msg.setDetailedText("The details are as follows:")
		msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
		msg.exec_()

	#####################################################################################################################################################################
	#genral methods

	def getSelectedRadioGroupBox(self, groupBox:QtWidgets.QGroupBox) -> QtWidgets.QRadioButton:
		#method to get the selected radio
		for child in groupBox.children():
			#verify if it is a radiobutton
			if isinstance(child, QtWidgets.QRadioButton):
				if child.isChecked():
					return child
		raise Exception('Radio button not found')

	def insertVariablesIntoTable(self, dictVar : Dict[str, str], graphicTable:QtWidgets.QTableWidget):
		#method to insert variable in in the table
		#clear the table
		graphicTable.setRowCount(0)
		for var in sorted(dictVar.keys()):
			graphicTable.setRowCount(graphicTable.rowCount() +1)
			graphicTable.setItem(graphicTable.rowCount()-1, 0, QtWidgets.QTableWidgetItem(var))
			graphicTable.setItem(graphicTable.rowCount()-1, 1, QtWidgets.QTableWidgetItem(str(dictVar[var])))

	def insertKleeneKnasterTarski(self, listWhile:List[List[str]]):
		#method to insert the kleene knaster taski iteration into tree widget
		#clear the widget
		self.ui.treeWidgetKleene.clear()
		for row in listWhile:
			self.ui.treeWidgetKleene.addTopLevelItem(QtWidgets.QTreeWidgetItem([row[0][3:], row[1][3:], row[2][4:], row[3]]))

	def copyDictFlotToStr(self, dictIn:Dict[str, float]) -> Dict[str, str]:
		#method to convert a dict and transform its element to string
		newDict = dict()
		for k in dictIn:
			newDict[k] = str(dictIn[k]).lower()
		return newDict

	def evaluateTree(self, 
					tree:Union[ArithmExpr, BoolExpr, Statement], 
					iniSt : Dict[str, float], 
					iniVarSet : Set[str], 
					flagRed:bool,
					typeParserGraphic:str) -> Tuple[Union[bool, float, None], List[str], List[List[str]], Dict[str, float], str]:
		#method to execute the tree
		listWhile = []
		listExec = []
		state = iniSt.copy()
		if flagRed:
			variableSet = iniVarSet.copy()
		else:
			variableSet = set()
		#execute with not Reduction
		if isinstance(tree, ArithmExpr):
			#evaluate process
			result = tree.evaluateProcess(state=state, listExecState=listExec, typeExpr= ExpressionBaseElement.typeEvalAExpr, flagAtomic=False, flagReduction=flagRed)
		elif isinstance(tree, BoolExpr):
			result = tree.evaluateProcess(state=state, listExecState=listExec, typeExpr= ExpressionBaseElement.typeEvalBExpr, flagAtomic=False, flagReduction=flagRed)
		elif isinstance(tree, Statement):
			result = tree.executionProcess(state=state, listShowWhile=listWhile, listExecState=listExec, flagReduction=flagRed, variableSet=variableSet, flagAtomic=False)
		#create the string for summarize the execution with Reduction flag = False
		stringExecution = self.toStrExecListDepth(listExecution=listExec, depth=0)
		stringWhile = ''.join(listWhile)
		listListWhile = [ i.split('\n') for i in listWhile ]
		typeParser = self.convertRadioParseStringToChar(typeParserGraphic=typeParserGraphic)
		stringSummarize = 'I(' + typeParser + ', ' + str(state) + ') -> ' + ('red' if flagRed else 'notRed') + ' = '
		if typeParserGraphic in (self.ui.radioButtonTypeAritExpr.text(), self.ui.radioButtonTypeBoolExpr.text()):
			stringSummarize += str(result).lower()
		elif typeParserGraphic == self.ui.radioButtonTypeStm.text():
			stringSummarize += str(result)
		#print(stringSummarize)
		completeString = '\n\nExecution with' + ('' if flagRed else 'out') +' reduce to simple case:\n' + stringSummarize + '\n\n' + stringExecution
		if len(stringWhile) >0:
			#there are some while statement
			completeString += '\n\nWhile statements:\n' + stringWhile
		#return the tuple
		return (result, listExec, listListWhile, state, completeString)

	def getInitialState(self) -> Dict[str, float]:
		#method to evaluate the initial state from graphic
		dictInitialState = dict()
		for r in range(self.ui.tableWidgetVariableInput.rowCount()):
			variableName = self.ui.tableWidgetVariableInput.item(r, 0).text()
			variableStrValue = self.ui.tableWidgetVariableInput.item(r, 1).text()
			try:
				#check if the variable is already in the dictionary
				if variableName in dictInitialState:
					raise Exception()
				dictInitialState[variableName] = float(variableStrValue.replace(',', '.'))
			except:
				#error in conversion a variable to number
				raise Exception(f'Error reading variable number {r+1}: {variableName}')
		return dictInitialState

	def parseStringInput(self) -> Tuple[str, str, Union[ArithmExpr, BoolExpr, Statement, None], str]:
		#method to parse the string
		#read the input string
		strInput = self.ui.plainTextEditStringToParse.toPlainText()
		#create the parser object
		parser = Parser()
		#parse the expression
		typeParser = self.getSelectedRadioGroupBox(groupBox=self.ui.groupBoxTypeParse).text()
		if typeParser == self.ui.radioButtonTypeAritExpr.text():
			#arithmetic expression
			(tree, remain) = parser.parseArithmExpr(string=strInput)
		elif typeParser == self.ui.radioButtonTypeBoolExpr.text():
			#boolean expression
			(tree, remain) = parser.parseBoolExpr(string=strInput)
		elif typeParser == self.ui.radioButtonTypeStm.text():
			#complete statement
			(tree, remain) = parser.parseStatement(string=strInput)
		else:
			raise Exception('Type of parser not valid')
		return (strInput, typeParser, tree, remain)

	def toStrExecListDepth(self, listExecution:list, depth:int=0) -> str:
		#method to return the string that represents the list in depth, every depth corresponding to an increment of tab
		strExec = ''
		for subList in listExecution:
			if type(subList) == list:
				#recursion
				strExec += self.toStrExecListDepth(listExecution=subList, depth=depth+1)
			else:
				#it is a string
				strExec += '\t'*(depth-1) + subList + '\n'
		return strExec

	def createFiles(self, tree:Union[ArithmExpr, BoolExpr, Statement], strInput:str, remain:str, typeParserGraphic:str, completeStringNotRed:str, completeStringRed:str):
		#method to write files
		#delete old files
		for i in os.listdir(constDirOut):
			pathFile = os.path.join(constDirOut, i)
			os.remove(pathFile)
		#populate the typeParser
		typeParser = self.convertRadioParseStringToChar(typeParserGraphic=typeParserGraphic)
		stringPlainTree = tree.toPlainString()
		stringTree = tree.toStringTree(nTab=0)
		baseStringSummarize = 'Input:\n' + strInput + '\n\nTree parsed:\n' + stringTree + '\n\nRemain string:\n' + ('**Nothing**' if remain=='' else remain) + '\n\nExpression:\n' + typeParser + ': ' + stringPlainTree
		#create the first file
		with open(constNameParserTree, 'w') as f:
			f.write(baseStringSummarize)
		#create the file without reduction
		#now the file can be created
		if len(completeStringNotRed) >0:
			with open(constNameSummarizeExecutionNotRed, 'w') as f:
				f.write(baseStringSummarize + completeStringNotRed)
		#file for reduction
		#now the file can be created
		if len(completeStringRed) > 0:
			with open(constNameSummarizeExecutionRed, 'w') as f:
				f.write(completeStringRed)

	def convertRadioParseStringToChar(self, typeParserGraphic:str) -> str:
		#method to convert the string from the radio to a char
		if typeParserGraphic == self.ui.radioButtonTypeAritExpr.text():
			typeParser = 'A'
		elif typeParserGraphic == self.ui.radioButtonTypeBoolExpr.text():
			typeParser = 'B'
		elif typeParserGraphic == self.ui.radioButtonTypeStm.text():
			typeParser = 'S'
		else:
			raise Exception('typeParserGraphic not valid')
		return typeParser

	def createChildrenExecution(self, listExecution:list) -> List[QtWidgets.QTreeWidgetItem]:
		#method to return the list that represents the list in depth, every depth corresponding to an increment of tab
		listChildren:List[QtWidgets.QTreeWidgetItem] = []
		for subList in listExecution:
			if type(subList) == list:
				#recursion
				listChildrenRecursion = self.createChildrenExecution(listExecution=subList)
				#add children to last child
				if len(listChildren) == 0:
					#extend the list
					listChildren.extend(listChildrenRecursion)
				else:
					#append
					listChildren[-1].addChildren(listChildrenRecursion)
				
			else:
				#it is a string
				#search for string, initial state and final state
				matchRe = re.match(pattern=r'([^{]*)\s*({[^}]*})\s*=\s*(.*)$', string=subList)
				stringExecution = matchRe.group(1)
				#initialState = eval(matchRe.group(2))
				initialStateStr = matchRe.group(2)
				resultStr = matchRe.group(3)
				#create the children
				#childInitialState = QtWidgets.QTreeWidgetItem([constStringInitialState, str(initialState)])
				#create the children for variable
				#listChildrenInitialState = self.createListChildrenTreeWidgetItemDict(dictVar=initialState)
				#childInitialState.addChildren(listChildrenInitialState)
				strFinalState = ''
				strFinalResult = ''
				#check if a { in the resultStr
				if '{' in resultStr:
					#it is a final state
					#finalState = eval(resultStr)
					#childFinalState = QtWidgets.QTreeWidgetItem([constStringFinalState, str(finalState)])
					#create the children for variable
					#listChildrenFinalState = self.createListChildrenTreeWidgetItemDict(dictVar=finalState)
					#childFinalState.addChildren(listChildrenFinalState)
					strFinalState = resultStr
				else:
					#is a simple result
					#childFinalState = QtWidgets.QTreeWidgetItem(['Result', resultStr])
					strFinalResult = resultStr
				#now create a item with the expression
				childEvaluation = QtWidgets.QTreeWidgetItem([stringExecution, initialStateStr, strFinalResult, strFinalState])
				#childEvaluation.addChildren([childInitialState, childFinalState])
				listChildren.append(childEvaluation)
		return listChildren

	def createListChildrenTreeWidgetItemDict(self, dictVar:Dict[str, float]) -> List[QtWidgets.QTreeWidgetItem]:
		#method to create the list of all children for the tree widget of state
		listChildren = []
		for k in sorted(dictVar.keys()):
			newItem = QtWidgets.QTreeWidgetItem([k, str(dictVar[k])])
			listChildren.append(newItem)
		return listChildren

	def insertExecutionStepIntoTree(self, listExecution:list, treeWidget:QtWidgets.QTreeWidget):
		#method to inser the steps into the tree
		#clear the tree
		treeWidget.clear()
		#create root children
		listRootChildren = self.createChildrenExecution(listExecution=listExecution)
		treeWidget.addTopLevelItems(listRootChildren)
		#treeWidget.expandAll()
		#expand only children different from final state and initial state
		for itemAdded in listRootChildren:
			self.expandOnlyExecutionItem(item=itemAdded)

	def expandOnlyExecutionItem(self, item:QtWidgets.QTreeWidgetItem):
		#recursion method to expand only item that are different from initial state o final state
		#verifiy the item
		if (item.text(0) not in (constStringFinalState, constStringInitialState)):
			item.setExpanded(True)
		for idChild in range(item.childCount()):
			self.expandOnlyExecutionItem(item=item.child(idChild))


