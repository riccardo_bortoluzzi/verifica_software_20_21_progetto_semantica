#script to implement the atomic boolean expression

from __future__ import annotations
from classes.ExpressionBaseElement import ExpressionBaseElement
from typing import Dict, List, Set, Union
import classes.BoolExpr as BoolExpr

import classes.ArithmBoolExpr as ArithmBoolExpr


class AtomBoolExpr(ExpressionBaseElement):
	#type to identify the atomic boolean expression
	typeFinalValue = 0 #final value like true and false
	typeArithm = 1
	typeNot = 2
	typeParenthesis = 3

	typeId : int
	finalValue : bool #final value in case of true and false
	arithmBoolExpr: ArithmBoolExpr.ArithmBoolExpr #arithmetic boolean expression 
	notBoolExpr: AtomBoolExpr #atomic boolean expression in case of not type
	innerBoolExpr : BoolExpr.BoolExpr #boolean expression in case of parenthesis

	def __init__(self, par: Union[bool, ArithmBoolExpr.ArithmBoolExpr, AtomBoolExpr, BoolExpr.BoolExpr]) -> None:
		#method to create a new object and identify the type
		super().__init__()
		if isinstance(par, bool):
			#final value
			self.typeId = self.typeFinalValue
			self.finalValue = par
		elif isinstance(par, ArithmBoolExpr.ArithmBoolExpr):
			#arithmetic boolean expression
			self.typeId = self.typeArithm
			self.arithmBoolExpr = par
		elif isinstance(par, AtomBoolExpr):
			#not expression
			self.typeId = self.typeNot
			self.notBoolExpr = par
		elif isinstance(par, BoolExpr.BoolExpr):
			#parenthesis expression
			self.typeId = self.typeParenthesis
			self.innerBoolExpr = par
		else:
			raise Exception('AtomBoolExpr with type par not valid')

	def toStringTree(self, nTab=0) -> str:
		#method to convert the boolean expression to a string
		#start with the number of tabs
		baseTab = '\t'*nTab
		stringTree = baseTab
		#identify the case
		if self.typeId == self.typeFinalValue:
			stringTree += 'Bool:' + str(self.finalValue) + '\n'
		elif self.typeId == self.typeArithm:
			stringTree = self.arithmBoolExpr.toStringTree(nTab=nTab)
		elif self.typeId == self.typeNot:
			stringTree += 'BExp[' + '\n'
			stringTree += baseTab + '\t' +'Not[' + '\n'
			stringTree += self.notBoolExpr.toStringTree(nTab=nTab+2)
			stringTree += baseTab + '\t' + ']' + '\n'
			stringTree += baseTab  + ']' + '\n'
		elif self.typeId == self.typeParenthesis:
			stringTree += 'BExp[' + '\n'
			stringTree += baseTab + '\t' +'(' + '\n'
			stringTree += self.innerBoolExpr.toStringTree(nTab=nTab+2)
			stringTree += baseTab + '\t' + ')' + '\n'
			stringTree += baseTab  + ']' + '\n'
		return stringTree

	def findVariableSet(self) -> Union[Set[str], None]:
		if self.typeId == self.typeFinalValue:
			return set()
		elif self.typeId == self.typeArithm:
			return self.arithmBoolExpr.findVariableSet()
		elif self.typeId == self.typeNot:
			return self.notBoolExpr.findVariableSet()
		elif self.typeId == self.typeParenthesis:
			return self.innerBoolExpr.findVariableSet()

	def evaluate(self, state: Dict[str, float], listExecState: Union[List[str], None], flagReduction: bool) -> Union[bool, None]:
		#method to evaluate the atomic boolean expression
		#the flag Reduction is ininfluent
		#distinguish the cases
		if self.typeId == self.typeFinalValue:
			return self.finalValue
		elif self.typeId == self.typeArithm:
			result = self.arithmBoolExpr.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalBExpr, flagAtomic=True, flagReduction=flagReduction)
			assert type(result) is bool
			return result
		elif self.typeId == self.typeNot:
			return not( self.notBoolExpr.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalBExpr, flagAtomic=False, flagReduction=flagReduction) )
		elif self.typeId == self.typeParenthesis:
			result = self.innerBoolExpr.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalBExpr, flagAtomic=False, flagReduction=flagReduction)
			assert type(result) is bool 
			return result

	#########################################################################################################################################################
	#method for graphic 

	def toGraphicTree(self):
		#method to convert the boolean expression to a graphic tree
		from PyQt5 import QtWidgets
		#identify the case
		if self.typeId == self.typeFinalValue:
			graphicTree = QtWidgets.QTreeWidgetItem()
			graphicTree.setText(0, 'Bool:' + str(self.finalValue))
		elif self.typeId == self.typeArithm:
			graphicTree = self.arithmBoolExpr.toGraphicTree()
		elif self.typeId == self.typeNot:
			graphicTree = QtWidgets.QTreeWidgetItem()
			graphicTree.setText(0, 'BExp')
			childTreeNot = QtWidgets.QTreeWidgetItem()
			childTreeNot.setText(0, 'Not')
			childTreeBoolExpr = self.notBoolExpr.toGraphicTree()
			childTreeNot.addChild(childTreeBoolExpr)
			graphicTree.addChild(childTreeNot)
		elif self.typeId == self.typeParenthesis:
			graphicTree = QtWidgets.QTreeWidgetItem()
			graphicTree.setText(0, 'BExp')
			childTreeFirstPar = QtWidgets.QTreeWidgetItem()
			childTreeFirstPar.setText(0, '(')
			graphicTree.addChild(childTreeFirstPar)
			childTreeExpr = self.innerBoolExpr.toGraphicTree()
			graphicTree.addChild(childTreeExpr)
			childTreeSecondPar = QtWidgets.QTreeWidgetItem()
			childTreeSecondPar.setText(0, ')')
			graphicTree.addChild(childTreeSecondPar)
		else:
			raise Exception ('Type AtomBoolExpr.typeId not valid')
		return graphicTree

