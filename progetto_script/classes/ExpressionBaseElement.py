#script to implement the base element for the expression with the method evaluate

from __future__ import annotations
from typing import Dict, List, Union
from classes.BaseElement import BaseElement


class ExpressionBaseElement(BaseElement):
	
	typeEvalVar = 'V'
	typeEvalAExpr = 'A'
	typeEvalBExpr = 'B'

	def __init__(self) -> None:
		super().__init__()

	def evaluateProcess(self, state: Dict[str, float], listExecState : Union[list, None], typeExpr:str, flagAtomic : bool, flagReduction:bool) -> Union[float, bool]:
		#method to do the entire process of evaluation
		#state: the current state at the begin of the execution
		#listExecState: if it is not none, it a list that contains the string of the statement, the string of the initial state and the string that rapresents the final state
		#typeExpr contains A if it is an arithmetic expression, B for boolean expression, V for vars
		#isAtomic: indicate if the espression is atomic, in this case it is not inserted into the list
		#flagReduction indicate if execute the expression with reductions (transform to base case) or with the best logic (execute immediate)
		listExecution = None
		initialState = None
		if flagAtomic:
			listExecution = listExecState
		elif listExecState is not None:
			listExecution = []
			initialState = state.copy()
		out = self.evaluate(state= state, listExecState = listExecution, flagReduction=flagReduction)
		if (listExecState is not None) and not(flagAtomic): #if it is atomic the execfution will be tracked in the next level
			#save the execution operation
			stringElement = self.toPlainString()
			stringExecution = typeExpr + '[[' + stringElement + ']] ' + str(initialState) + ' = ' + str(out).lower()
			assert listExecution is not None
			listExecution.insert(0, stringExecution)
			listExecState.append(listExecution)
		return out

	def evaluate(self, state: Dict[str, float], listExecState : Union[List[str], None],  flagReduction:bool) -> Union[float, bool]:
		#method to evaluate the expression
		#will be overwritten by the single entity
		#to avoid not implementation this method raise exception
		raise Exception('method evaluate not implemented')

	
