#script to implement the atomic arithmetic expression

from __future__ import annotations
from classes.ExpressionBaseElement import ExpressionBaseElement

from typing import Dict, List, Set, Tuple, Union
import classes.ArithmExpr as ArithmExpr
from classes.Variable import Variable

class AtomArithmExpr(ExpressionBaseElement):
	#id to distinguish which element contains
	typeNumeral = 0
	typeVariable = 1
	typeParenthesis = 2

	idType : int
	numeral : float #if the expression is a numeral
	variable : Variable #variable if the expression is a variable
	innerExpr : ArithmExpr.ArithmExpr #if the expression is an arithmetic expression inside parenthesis

	def __init__(self, par:Union[float, Variable, ArithmExpr.ArithmExpr ] ) -> None:
		#control in which case we are
		super().__init__()
		if isinstance(par, float):
			self.idType = self.typeNumeral
			self.numeral = par
		elif isinstance(par, Variable):
			self.idType = self.typeVariable
			self.variable = par
		elif isinstance(par, ArithmExpr.ArithmExpr):
			self.idType = self.typeParenthesis
			self.innerExpr = par
		else:
			#error
			raise Exception('Input AtomArithmExpr.ArithmExpr not valid')

	def toStringTree(self, nTab=0) -> str:
		#method to convert the atomic aritmetic expression to a string
		#start with the number of tabs
		baseTab = '\t'*nTab
		stringTree = baseTab
		if self.idType == self.typeNumeral:
			stringTree += 'Num:' +  str(self.numeral) + '\n'
		elif self.idType == self.typeVariable:
			#in this case the string is replaced, because the number of tab is inside the method of variables
			stringTree = self.variable.toStringTree(nTab=nTab)
		elif self.idType == self.typeParenthesis:
			stringTree += 'AExp[' + '\n'
			stringTree += baseTab + '\t' '(' + '\n'
			stringTree += self.innerExpr.toStringTree(nTab=nTab+2)
			stringTree += baseTab + '\t' + ')' + '\n'
			stringTree += baseTab + ']' + '\n'
		else:
			#error
			raise Exception('Type of ArithmExpr not found')
		#return the string
		return stringTree

	def findVariableSet(self) -> Union[Set[str], None]:
		if self.idType == self.typeNumeral:
			return set()
		elif self.idType == self.typeVariable:
			return self.variable.findVariableSet()
		elif self.idType == self.typeParenthesis:
			return self.innerExpr.findVariableSet()

	def evaluate(self, state: Dict[str, float], listExecState: Union[List[str], None], flagReduction: bool) -> Union[float, None]:
		#method to evaluate the atomic arithmetic expression, the flagReduction is ininfluent
		if self.idType == self.typeNumeral:
			#it is a numeral
			return self.numeral
		elif self.idType == self.typeVariable:
			#it is a variable
			return self.variable.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalVar, flagAtomic=False, flagReduction=flagReduction)
		elif self.idType == self.typeParenthesis:
			#it an expression encloseb into parenthesis
			return self.innerExpr.evaluateProcess(state=state, listExecState=listExecState, typeExpr=ExpressionBaseElement.typeEvalAExpr, flagAtomic=False, flagReduction=flagReduction)

	#########################################################################################################################################################
	#method for graphic 

	def toGraphicTree(self):
		#method to convert the atomic aritmetic expression to a graphic tree
		from PyQt5 import QtWidgets
		
		if self.idType == self.typeNumeral:
			graphicTree = QtWidgets.QTreeWidgetItem()
			graphicTree.setText(0, 'Num')
			numChildTree = QtWidgets.QTreeWidgetItem()
			numChildTree.setText(0, str(self.numeral))
			graphicTree.addChild(numChildTree)
		elif self.idType == self.typeVariable:
			#in this case the string is replaced, because the number of tab is inside the method of variables
			graphicTree = self.variable.toGraphicTree()
		elif self.idType == self.typeParenthesis:
			graphicTree = QtWidgets.QTreeWidgetItem()
			graphicTree.setText(0, 'AExp')
			childTreeFirstPar = QtWidgets.QTreeWidgetItem()
			childTreeFirstPar.setText(0, '(')
			graphicTree.addChild(childTreeFirstPar)
			childTreeExpr = self.innerExpr.toGraphicTree()
			graphicTree.addChild(childTreeExpr)
			childTreeSecondPar = QtWidgets.QTreeWidgetItem()
			childTreeSecondPar.setText(0, ')')
			graphicTree.addChild(childTreeSecondPar)
		else:
			#error
			raise Exception('Type of ArithmExpr not found')
		#return the string
		return graphicTree
		