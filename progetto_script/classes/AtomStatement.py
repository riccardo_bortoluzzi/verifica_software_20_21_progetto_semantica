#script to implement the atomic statement

from __future__ import annotations
from classes.StatementBaseElement import StatementBaseElement

from typing import Dict, List, Set, Union
import classes.AssignmentStm as AssignmentStm 
import classes.SkipStm as SkipStm
import classes.IfStm as IfStm
import classes.WhileStm as WhileStm
import classes.RepeatStm as RepeatStm
import classes.ForStm as ForStm

class AtomStatement(StatementBaseElement):
	#type to identify the case of the atomic statement
	typeAssignament = 0
	typeSkip = 1
	typeIf = 2
	typeWhile = 3
	typeRepeat = 4
	typeFor = 5

	typeId : int
	assStm : AssignmentStm.AssignmentStm #assignment statement
	skipStm : SkipStm.SkipStm #skip statement
	ifStm : IfStm.IfStm #if statement
	whileStm : WhileStm.WhileStm #while statement
	repeatStm : RepeatStm.RepeatStm #repeat until statement
	forStm : ForStm.ForStm #for statement

	def __init__(self, par:Union[AssignmentStm.AssignmentStm, SkipStm.SkipStm, IfStm.IfStm, WhileStm.WhileStm, RepeatStm.RepeatStm, ForStm.ForStm]) -> None:
		#method to inizialize the atomic statement 
		super().__init__()
		if isinstance(par, AssignmentStm.AssignmentStm):
			self.typeId = self.typeAssignament
			self.assStm = par
		elif isinstance(par, SkipStm.SkipStm):
			self.typeId = self.typeSkip
			self.skipStm = par
		elif isinstance(par, IfStm.IfStm):
			self.typeId = self.typeIf
			self.ifStm = par
		elif isinstance(par, WhileStm.WhileStm):
			self.typeId = self.typeWhile
			self.whileStm = par
		elif isinstance(par, RepeatStm.RepeatStm):
			self.typeId = self.typeRepeat
			self.repeatStm = par
		elif isinstance(par, ForStm.ForStm):
			self.typeId = self.typeFor
			self.forStm = par
		else:
			raise Exception('AtomStatement with wrong parameter')

	def toStringTree(self, nTab=0) -> str:
		#method to convert the atomic statement to a string
		#start with the number of tabs
		baseTab = '\t'*nTab
		stringTree = baseTab + 'Stm[' + '\n'
		if self.typeId == self.typeAssignament:
			stringTree += self.assStm.toStringTree(nTab=nTab+1)
		elif self.typeId == self.typeSkip:
			stringTree += self.skipStm.toStringTree(nTab=nTab+1)
		elif self.typeId == self.typeIf:
			stringTree += self.ifStm.toStringTree(nTab=nTab+1)
		elif self.typeId == self.typeWhile:
			stringTree += self.whileStm.toStringTree(nTab=nTab+1)
		elif self.typeId == self.typeRepeat:
			stringTree += self.repeatStm.toStringTree(nTab=nTab+1)
		elif self.typeId == self.typeFor:
			stringTree += self.forStm.toStringTree(nTab=nTab+1)
		#close the tree
		stringTree += baseTab + ']' + '\n'
		return stringTree

	def findVariableSet(self) -> Union[Set[str], None]:
		if self.typeId == self.typeAssignament:
			return self.assStm.findVariableSet()
		elif self.typeId == self.typeSkip:
			return self.skipStm.findVariableSet()
		elif self.typeId == self.typeIf:
			return self.ifStm.findVariableSet()
		elif self.typeId == self.typeWhile:
			return self.whileStm.findVariableSet()
		elif self.typeId == self.typeRepeat:
			return self.repeatStm.findVariableSet()
		elif self.typeId == self.typeFor:
			return self.forStm.findVariableSet()

	def execute(self, state: Dict[str, float], listShowWhile: Union[List[str], None], listExecState: Union[List[str], None], flagReduction: bool, variableSet: Set[str]) -> None:
		#the execution is different only for while statement at the first iteration
		flagShowWhile = False
		initialState = dict()
		if (self.typeId==self.typeWhile) and (listShowWhile is not None) and (self.whileStm.numberOfExecution==0):
			initialState = state.copy()
			flagShowWhile = True
		elif (self.typeId in (self.typeRepeat, self.typeFor)) and (flagReduction==False):
			#also the repeat until statement has the number of execution 
			initialState = state.copy()
			flagShowWhile = True
		#identify the case
		if self.typeId == self.typeAssignament:
			self.assStm.executionProcess(state=state, listShowWhile=listShowWhile, listExecState=listExecState, flagReduction=flagReduction, variableSet=variableSet, flagAtomic=False)
		elif self.typeId == self.typeSkip:
			self.skipStm.executionProcess(state=state, listShowWhile=listShowWhile, listExecState=listExecState, flagReduction=flagReduction, variableSet=variableSet, flagAtomic=False)
		elif self.typeId == self.typeIf:
			self.ifStm.executionProcess(state=state, listShowWhile=listShowWhile, listExecState=listExecState, flagReduction=flagReduction, variableSet=variableSet, flagAtomic=False)
		elif self.typeId == self.typeWhile:
			self.whileStm.executionProcess(state=state, listShowWhile=listShowWhile, listExecState=listExecState, flagReduction=flagReduction, variableSet=variableSet, flagAtomic=False)
		elif self.typeId == self.typeRepeat:
			self.repeatStm.executionProcess(state=state, listShowWhile=listShowWhile, listExecState=listExecState, flagReduction=flagReduction, variableSet=variableSet, flagAtomic=False)
		elif self.typeId == self.typeFor:
			self.forStm.executionProcess(state=state, listShowWhile=listShowWhile, listExecState=listExecState, flagReduction=flagReduction, variableSet=variableSet, flagAtomic=False)
		#verify if it is necessary to insert the while execution
		if flagShowWhile:
			#prepare the string
			stringWhile = 'F: '+ self.toPlainString()
			stringInitialState = 's: '+str(initialState)
			stringFinalState = "s': " + str(state)
			stringKleene = ''
			if self.typeId == self.typeWhile:
				stringKleene = 'F^' + str(self.whileStm.numberOfExecution) + "(⊥) s = s'"
			elif self.typeId == self.typeRepeat:
				stringKleene = 'F^' + str(self.repeatStm.numberOfExecution) + "(⊥) s = s'"
			elif self.typeId == self.typeFor:
				stringKleene = 'F^' + str(self.forStm.numberOfExecution) + "(⊥) s = s'"
			completeString = stringWhile + '\n' + stringInitialState + '\n' + stringFinalState + '\n' + stringKleene + '\n\n'
			assert listShowWhile is not None
			listShowWhile.append(completeString)
			#now ripristinate the number of execution to avoid a copy method and reuse the while statement (in the repeat until)
			if self.typeId == self.typeWhile:
				self.whileStm.numberOfExecution = 0
			elif self.typeId == self.typeRepeat:
				self.repeatStm.numberOfExecution = 0
			elif self.typeId == self.typeFor:
				self.forStm.numberOfExecution = 0

	#########################################################################################################################################################
	#method for graphic 

	def toGraphicTree(self):
		#method to convert the atomic statement to a graphic tree
		from PyQt5 import QtWidgets
		graphicTree = QtWidgets.QTreeWidgetItem()
		graphicTree.setText(0, 'Stm')
		if self.typeId == self.typeAssignament:
			childTree = self.assStm.toGraphicTree()
			graphicTree.addChild(childTree)
		elif self.typeId == self.typeSkip:
			childTree = self.skipStm.toGraphicTree()
			graphicTree.addChild(childTree)
		elif self.typeId == self.typeIf:
			childTree = self.ifStm.toGraphicTree()
			graphicTree.addChild(childTree)
		elif self.typeId == self.typeWhile:
			childTree = self.whileStm.toGraphicTree()
			graphicTree.addChild(childTree)
		elif self.typeId == self.typeRepeat:
			childTree = self.repeatStm.toGraphicTree()
			graphicTree.addChild(childTree)
		elif self.typeId == self.typeFor:
			childTree = self.forStm.toGraphicTree()
			graphicTree.addChild(childTree)
		#close the tree
		return graphicTree
		

