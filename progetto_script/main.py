#script with the main of the parser

from classes.Parser import Parser
from classes.ExpressionBaseElement import ExpressionBaseElement
from classes.Statement import Statement
from classes.BoolExpr import BoolExpr
from classes.ArithmExpr import ArithmExpr
import os, re
from typing import Dict, List, Set, Tuple, Union

from classes.Constants import constDirOut, constNameParserTree, constNameSummarizeExecutionNotRed, constNameSummarizeExecutionRed

#constants


constTypeArithmeticExpression = 'A'
constTypeBoolExpression = 'B'
constTypeStatement = 'S'

constTypeReduction = 'R'
constTypeNotReduction = 'N'
constTypeAllReduction = 'A'

constTypeFile = 'F'
constTypeInput = 'I'

def toStrExecListDepth(listExecution:list, depth:int=0) -> str:
	#function to return the string that represents the list in depth, every depth corresponding to an increment of tab
	strExec = ''
	for subList in listExecution:
		if type(subList) == list:
			#recursion
			strExec += toStrExecListDepth(listExecution=subList, depth=depth+1)
		else:
			#it is a string
			strExec += '\t'*(depth-1) + subList + '\n'
	return strExec

def evaluateTree(tree:Union[ArithmExpr, BoolExpr, Statement], iniSt : Dict[str, float], iniVarSet : Set[str], flagRed:bool) -> Tuple[Union[bool, float, None], List[str], List[str], Dict[str, float]]:
	#function to execute the tree
	listWhile = []
	listExec = []
	state = iniSt.copy()
	if flagRed:
		variableSet = iniVarSet.copy()
	else:
		variableSet = set()
	#execute with not Reduction
	if isinstance(tree, ArithmExpr):
		#evaluate process
		result = tree.evaluateProcess(state=state, listExecState=listExec, typeExpr= ExpressionBaseElement.typeEvalAExpr, flagAtomic=False, flagReduction=flagRed)
	elif isinstance(tree, BoolExpr):
		result = tree.evaluateProcess(state=state, listExecState=listExec, typeExpr= ExpressionBaseElement.typeEvalBExpr, flagAtomic=False, flagReduction=flagRed)
	elif isinstance(tree, Statement):
		#assert variableSet is not None
		result = tree.executionProcess(state=state, listShowWhile=listWhile, listExecState=listExec, flagReduction=flagRed, variableSet=variableSet, flagAtomic=False)
	#return the tuple
	return (result, listWhile, listExec, state)

def parseExecuteProcess(strInput:str, typeParser:str, typeRed:str, state:Dict[str, float]) -> None:
	#function to execute the test
	#delete old files
	for i in os.listdir(constDirOut):
		pathFile = os.path.join(constDirOut, i)
		os.remove(pathFile)
	#create the parser object
	parser = Parser()
	#parse the expression
	if typeParser == constTypeArithmeticExpression:
		(tree, remain) = parser.parseArithmExpr(string=strInput)
	elif typeParser == constTypeBoolExpression:
		(tree, remain) = parser.parseBoolExpr(string=strInput)
	elif typeParser == constTypeStatement:
		(tree, remain) = parser.parseStatement(string=strInput)
	else:
		raise Exception('Type of parser not valid')
	#create the string for the files
	assert tree is not None
	stringTree = tree.toStringTree(nTab=0)
	stringPlainTree = tree.toPlainString()
	baseStringSummarize = 'Input:\n' + strInput + '\n\nTree parsed:\n' + stringTree + '\n\nRemain string:\n' + ('**Nothing**' if remain=='' else remain) + '\n\nExpression:\n' + typeParser + ': ' + stringPlainTree
	#create the first file
	with open(constNameParserTree, 'w') as f:
		f.write(baseStringSummarize)
	#if remains is not null raise exception
	if len(remain) != 0:
		raise Exception('Syntax error: ' + remain)
	print('\n\nParse complete, parsed tree:\n' + stringTree)
	#prepare the execution
	if typeRed in (constTypeNotReduction, constTypeAllReduction):
		#without Reduction flag
		(resultNotRed, listWhileNotRed, listExecNotRed, stateNotRed) = evaluateTree(tree=tree, iniSt=state, iniVarSet=set(), flagRed=False)
		#create the string for summarize the execution with Reduction flag = False
		stringExecutionNotRed = toStrExecListDepth(listExecution=listExecNotRed, depth=0)
		stringWhileNotRed = ''.join(listWhileNotRed)
		stringSummarizeNotRed = 'I(' + typeParser + ', ' + str(state) + ') -> notRed = '
		if typeParser in (constTypeArithmeticExpression, constTypeBoolExpression):
			stringSummarizeNotRed += str(resultNotRed).lower()
		elif typeParser == constTypeStatement:
			stringSummarizeNotRed += str(stateNotRed)
		print(stringSummarizeNotRed)
		completeStringNotRed = baseStringSummarize + '\n\nExecution without reduce to simple case:\n' + stringSummarizeNotRed + '\n\n' + stringExecutionNotRed
		if len(stringWhileNotRed) >0:
			#there are some while statement
			completeStringNotRed += '\n\nWhile statements:\n' + stringWhileNotRed
		#now the file can be created
		with open(constNameSummarizeExecutionNotRed, 'w') as f:
			f.write(completeStringNotRed)
	if typeRed in (constTypeReduction, constTypeAllReduction):
		#the evaluation with Reduction reductions
		initialVariableSet = tree.findVariableSet()
		(resultRed, listWhileRed, listExecRed, stateRed) = evaluateTree(tree=tree, iniSt=state, iniVarSet=initialVariableSet, flagRed=True)
		#create the string for summarize the execution with Reduction flag = True
		stringExecutionRed = toStrExecListDepth(listExecution=listExecRed, depth=0)
		stringWhileRed = ''.join(listWhileRed)
		stringSummarizeRed = 'I(' + typeParser + ', ' + str(state) + ') -> red = '
		if typeParser in (constTypeArithmeticExpression, constTypeBoolExpression):
			stringSummarizeRed += str(resultRed).lower()
		elif typeParser == constTypeStatement:
			stringSummarizeRed += str(stateRed)
		print(stringSummarizeRed)
		completeStringRed = baseStringSummarize + '\n\nExecution with reduction to simple case:\n' + stringSummarizeRed + '\n\n' + stringExecutionRed
		if len(stringWhileRed) >0:
			#there are some while statement
			completeStringRed += '\n\nWhile statements:\n' + stringWhileRed
		#now the file can be created
		with open(constNameSummarizeExecutionRed, 'w') as f:
			f.write(completeStringRed)
	print('Execution complete')
	

def checkInput(permissedValues : List[str]) -> str:
	#function to verify if the user insert a valid char
	stringShow = 'Insert a value between ' + ', '.join(permissedValues) + ': '
	charInserted = input(stringShow).upper()
	while charInserted not in permissedValues:
		print('Not valid')
		charInserted = input(stringShow).upper()
	return charInserted


def main():
	#function main
	#ask for type to parse
	print(f'You can parse arithmetic expression ({constTypeArithmeticExpression}), boolean expression ({constTypeBoolExpression}) or complete statement ({constTypeStatement})')
	typeToParse = checkInput(permissedValues=[constTypeArithmeticExpression, constTypeBoolExpression, constTypeStatement])
	#ask if the user want to parse a file or a input string
	print(f'You can parse an entire file ({constTypeFile}) or a single statement ({constTypeInput})')
	fromRead = checkInput(permissedValues=[constTypeFile, constTypeInput])
	if fromRead.strip() == constTypeFile:
		#ask the path for the file
		pathFile = input('Insert the path of the file: ')

		while not(os.path.exists(pathFile)) or not(os.path.isfile(pathFile)):
			print('Path not valid')
			pathFile = input('Insert the path of the file: ')
		#read the file
		with open(pathFile, 'r') as f:
			stringToParse = f.read().replace('\n', ' ')
	else:
		#ask the expression
		#ask for input
		stringToParse = input('Insert the string to parse: ')
	#ask for Reduction or not
	print(f'''You can evaluate the expression: 
	with reductions to simple case ({constTypeReduction}) [with no terminations statements it raises exception of maximum of recursion]
	with the minimum number of steps to evaluate the expression ({constTypeNotReduction}) [in case of non termination, the program will run forever]
	with both of these ({constTypeAllReduction})''')
	typeRed = checkInput(permissedValues=[constTypeReduction, constTypeNotReduction, constTypeAllReduction])
	#ask for the initial state
	strInitialState = input('Insert the initial state of type var:float (ex: x:5.6, a:10, b:0.001): ')
	initialStateDict = {}
	for couple in re.split(pattern=r'\s*,\s*', string=strInitialState):
		if couple != '':
			(variableName, strValue) = re.split(pattern=r'\s*:\s*', string=couple)
			initialStateDict[variableName] = float(strValue)
	#call the function
	parseExecuteProcess(strInput=stringToParse, typeParser=typeToParse, typeRed=typeRed, state=initialStateDict)

if __name__ == '__main__':
	main()
	

